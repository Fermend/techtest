import Vue from 'vue'
import Router from 'vue-router'
import HomeForm from '@/components/Home/HomeForm'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HomeForm',
      component: HomeForm
    }
  ],
  mode: 'history'
})
